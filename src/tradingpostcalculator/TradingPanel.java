package tradingpostcalculator;

import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author David Medenjak 0932061
 * @since 30.12.2012
 */
public class TradingPanel extends JPanel {

  public TradingPanel() {
    this(new GridLayout(0,2));
  }

  private JTextField buyField;
  private JTextField sellField;
  
  public TradingPanel(LayoutManager layout) {
    super(layout);
    buyField = new JTextField();
    sellField = new JTextField();
    add(buyField);
    add(sellField);
  }
  
  
}
