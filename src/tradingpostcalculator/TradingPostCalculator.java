package tradingpostcalculator;

import javax.swing.JFrame;

/**
 *
 * @author David
 */
public class TradingPostCalculator  {
  private JFrame frame;
  private TradingPanel tradingPanel = new TradingPanel();
  
  public TradingPostCalculator() {
    this.frame = new JFrame("Trading Post Calculator");
    frame.setAlwaysOnTop( true );

        frame.setLocationByPlatform( true );
        frame.add(tradingPanel);
        frame.pack();
        frame.setVisible( true );
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
  
  

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    TradingPostCalculator tpc = new TradingPostCalculator();
  }
}
